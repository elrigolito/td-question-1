package fr.eilco.brice.td;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private EditText etGauche;
    private EditText etDroit;
    private TextView tvResult;
    private TextView tvResult2;
    private TextView tvResult3;
    private TextView tvResult4;
    private Button bouton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        etGauche = (EditText) findViewById(R.id.idEditText);
        etDroit = (EditText) findViewById(R.id.editText6);
        tvResult = (TextView) findViewById(R.id.textView3);
        tvResult2 = (TextView) findViewById(R.id.textView4);
        tvResult3 = (TextView) findViewById(R.id.textView5);
        tvResult4 = (TextView) findViewById(R.id.textView6);
        bouton = (Button) findViewById(R.id.button3);

        bouton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Editable prems =etGauche.getText();
                Editable deuz = etDroit.getText();
                int addi=Integer.parseInt(prems.toString())+Integer.parseInt(deuz.toString());
                int sous=Integer.parseInt(prems.toString())-Integer.parseInt(deuz.toString());
                int mult=Integer.parseInt(prems.toString())*Integer.parseInt(deuz.toString());
                if(Integer.parseInt(deuz.toString()) != 0){
                    int div=Integer.parseInt(prems.toString())/Integer.parseInt(deuz.toString());

                    tvResult4.setText("Résultat de la division : "+Integer.toString(div));
                }
                else{
                    tvResult4.setText("Impossible : division par 0");
                }
                tvResult.setText("Résultat de l'addition : "+Integer.toString(addi));
                tvResult2.setText("Résultat de la soustraction : "+Integer.toString(sous));
                tvResult3.setText("Résultat de la mutliplication : "+Integer.toString(mult));

            }
        });



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
